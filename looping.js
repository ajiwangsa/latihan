// Looping While
console.log('LOOPING PERTAMA')
var loop=2;
while(loop<=20){
	console.log(loop+' - I love coding')
	loop += 2
}
console.log('LOOPING KEDUA')
while(loop>2){
	loop -= 2
	console.log(loop+' - I will become a mobile developer')
}

// Looping menggunakan for
console.log('OUTPUT')
for(var i=1;i<=20;i++){
	if(i%3==0 && i%2==1){
		console.log(i + ' - I Love Coding')
	}else if(i%2==0){
		console.log(i + ' - Berkualitas')
	}else{
		console.log(i + ' - Santai')
	}
}

// Membuat Persegi Panjang
var i = 0
while(i<4){
	var pagar='#'
	for(var p=1;p<=8;p++){
		pagar=pagar+'#'
	}
	console.log(pagar)
	i++
}

// Membuat tangga
var pager = '#'
var i = 0
while(i<7){
	console.log(pager);
	pager=pager+'#';
	i++
}

// Membuat papan catur
var pagar2 = '#'
var spasi = ' '
i = 0
while(i<8){
	var temppagar2 = pagar2
	var tempspasi = spasi
	var catur = ''
	for(var p=0;p<4;p++){
		catur=catur+tempspasi
		catur=catur+temppagar2
	}
	pagar2=tempspasi
	spasi=temppagar2
	console.log(catur)
	i++
}
